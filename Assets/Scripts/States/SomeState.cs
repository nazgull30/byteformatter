﻿using ByteFormatter.Runtime.StateSerialize;

namespace States
{
	[ByteDataContext(nameof(SomeState))]
	public partial class SomeState
	{
		[ByteDataIndex(0)] public int Value { get; set; }
		public NotSerializedData Data { get; set; }
	}

	public class NotSerializedData
	{
	}
}