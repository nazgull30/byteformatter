﻿using UnityEngine;

namespace ByteFormatter.Runtime.MigrateHelper.Impls
{
	[ByteDataTypeAccess(typeof(Quaternion?))]
	public sealed class QuaternionNullableByteData : AByteData<QuaternionNullableByteData>
	{
		public override void Transfer(ByteReader reader, ByteWriter writer)
			=> writer.Write(reader.ReadQuaternionNullable());

		public override void Skip(ByteReader reader) => reader.SkipQuaternionNullable();
	}
}