﻿using UnityEngine;

namespace ByteFormatter.Runtime.MigrateHelper.Impls
{
	[ByteDataTypeAccess(typeof(Bounds))]
	public class BoundsByteData : AByteData<BoundsByteData>
	{
		public override void Transfer(ByteReader reader, ByteWriter writer)
			=> writer.Write(reader.ReadBounds());

		public override void Skip(ByteReader reader)
			=> reader.SkipBounds();
	}
}