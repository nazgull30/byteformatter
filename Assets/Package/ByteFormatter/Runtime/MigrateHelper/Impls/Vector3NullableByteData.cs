﻿using UnityEngine;

namespace ByteFormatter.Runtime.MigrateHelper.Impls
{
	[ByteDataTypeAccess(typeof(Vector3?))]
	public sealed class Vector3NullableByteData : AByteData<Vector3NullableByteData>
	{
		public override void Transfer(ByteReader reader, ByteWriter writer)
			=> writer.Write(reader.ReadVector3Nullable());

		public override void Skip(ByteReader reader) => reader.SkipVector3Nullable();
	}
}