﻿using UnityEngine;

namespace ByteFormatter.Runtime.MigrateHelper.Impls
{
	[ByteDataTypeAccess(typeof(Quaternion))]
	public sealed class QuaternionByteData : AByteData<QuaternionByteData>
	{
		public override void Transfer(ByteReader reader, ByteWriter writer)
			=> writer.Write(reader.ReadQuaternion());

		public override void Skip(ByteReader reader) => reader.SkipQuaternion();
	}
}