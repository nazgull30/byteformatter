﻿using System.Collections.Generic;

namespace Package.ByteFormatter.VersionDiff
{
	public class Diff
	{
		public string Name;
		public EDiffType DiffType;
		public List<Diff> ChildDiffs;

		public Diff()
		{
		}

		public Diff(string name, EDiffType diffType) : this(name, diffType, new List<Diff>())
		{
		}

		public Diff(string name, EDiffType diffType, List<Diff> childDiffs)
		{
			Name = name;
			DiffType = diffType;
			ChildDiffs = childDiffs;
		}
	}
}