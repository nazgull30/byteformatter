﻿using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using ByteFormatter.Runtime.MigrateHelper;
using Package.ByteFormatter.Generator.Utils;
using Package.ByteFormatter.VersionDiff.Contexts;

namespace Package.ByteFormatter.VersionDiff
{
	public static class ByteDataDiffService
	{
		public static List<VersionData> GetVersions()
			=> AssemblyHelper.GetTypes()
				.Where(f => f.GetCustomAttribute<DataVersionAttribute>() != null)
				.GroupBy(type => type.GetCustomAttribute<DataVersionAttribute>(), type => type)
				.Select(group => new VersionData
				{
					Version = group.Key.Version,
					Contexts = group
						.Select(s => new StateDataContext(group.Key.TypeName, s))
						.Cast<ADataContext>()
						.ToList()
				}).ToList();

		public static List<Diff> Diff(string fromVersion, string toVersion)
		{
			var versionDatas = GetVersions();
			var fromVersionData = versionDatas.Find(f => f.Version == fromVersion);
			var toVersionData = versionDatas.Find(f => f.Version == toVersion);
			var diffs = new List<Diff>();

			foreach (var fromContext in fromVersionData.Contexts)
			{
				var toContext = toVersionData.Contexts.Find(f => f.Equals(fromContext));
				var diff = toContext != null
					? Diff(fromContext, toContext)
					: Diff(fromContext, ADataContext.Empty);
				diffs.Add(diff);
			}

			foreach (var context in toVersionData.Contexts.Except(fromVersionData.Contexts))
			{
				diffs.Add(Diff(ADataContext.Empty, context));
			}

			return diffs;
		}

		private static Diff Diff(ADataContext context1, ADataContext context2)
		{
			var diffs = new List<Diff>();
			var childContexts1 = context1.ChildContexts;
			var childContexts2 = context2.ChildContexts;
			diffs.AddRange(FindAdded(childContexts1, childContexts2));
			diffs.AddRange(FindRemoved(childContexts1, childContexts2));
			diffs.AddRange(FindChanged(childContexts1, childContexts2));
			diffs.AddRange(FindMoved(childContexts1, childContexts2));

			if (diffs.Count == 0)
				return new Diff(context2.Name, EDiffType.None);

			if (context1.Equals(ADataContext.Empty))
				return new Diff(context2.Name, EDiffType.Added, diffs);

			if (context2.Equals(ADataContext.Empty))
				return new Diff(context1.Name, EDiffType.Removed, diffs);

			return new Diff(context2.Name, EDiffType.Changed, diffs);
		}

		private static List<Diff> FindAdded(List<ADataContext> contexts1, List<ADataContext> contexts2)
			=> contexts2.Except(contexts1)
				.Select(s => new Diff
				{
					Name = s.Name,
					DiffType = EDiffType.Added,
					ChildDiffs = FindAdded(new List<ADataContext>(), s.ChildContexts)
				})
				.ToList();

		private static List<Diff> FindRemoved(List<ADataContext> contexts1, List<ADataContext> contexts2)
			=> contexts1.Except(contexts2)
				.Select(s => new Diff
				{
					Name = s.Name,
					DiffType = EDiffType.Removed,
					ChildDiffs = FindRemoved(s.ChildContexts, new List<ADataContext>())
				})
				.ToList();

		private static List<Diff> FindChanged(List<ADataContext> contexts1, List<ADataContext> contexts2)
		{
			var changed = new List<Diff>();
			for (var i = 0; i < contexts1.Count && i < contexts2.Count; i++)
			{
				var context1 = contexts1[i];
				var context2 = contexts2[i];
				if (!context1.Equals(context2))
					continue;

				var diff = Diff(context1, context2);
				if (diff.DiffType == EDiffType.None)
					continue;

				changed.Add(diff);
			}

			return changed;
		}

		private static List<Diff> FindMoved(List<ADataContext> contexts1, List<ADataContext> contexts2)
		{
			var changed = new List<Diff>();
			for (var i = 0; i < contexts1.Count; i++)
			{
				var context1 = contexts1[i];
				var index = contexts2.FindIndex(f => context1.Equals(f));
				if (index == -1 || index == i)
					continue;

				var context2 = contexts2[index];
				var diff = new Diff(context1.Name, EDiffType.Moved);
				var diffChild = Diff(context1, context2);
				if (diff.DiffType != EDiffType.None)
					diff.ChildDiffs = diffChild.ChildDiffs;

				changed.Add(diff);
			}

			return changed;
		}
	}
}