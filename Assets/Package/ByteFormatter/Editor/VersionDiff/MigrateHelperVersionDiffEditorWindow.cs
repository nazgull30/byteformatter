﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace Package.ByteFormatter.VersionDiff
{
	public class MigrateHelperVersionDiffEditorWindow : EditorWindow
	{
		private static readonly Dictionary<EDiffType, string> DiffPrefixes = new Dictionary<EDiffType, string>
		{
			{EDiffType.None, string.Empty},
			{EDiffType.Added, "A: "},
			{EDiffType.Changed, "C: "},
			{EDiffType.Moved, "M: "},
			{EDiffType.Removed, "R: "}
		};

		private List<VersionData> _versionDatas;

		private string[] _versions;
		private int _fromVersion;
		private int _toVersion;
		private List<Diff> _diffs;

		private Vector2 _scrollPosition;

		[MenuItem("Tools/Migrator/Version Diff")]
		public static void OpenSettingsWindowButton()
		{
			var window = GetWindow<MigrateHelperVersionDiffEditorWindow>(false, "Migrator");
			window.Show();
		}

		private void OnEnable()
		{
			_versionDatas = ByteDataDiffService.GetVersions();
			_versions = _versionDatas.Select(s => s.Version).ToArray();
		}

		private void OnGUI()
		{
			GUILayout.Space(5);
			GUILayout.BeginHorizontal();
			GUILayout.Label("From");
			_fromVersion = EditorGUILayout.Popup(_fromVersion, _versions);
			GUILayout.Label("To");
			_toVersion = EditorGUILayout.Popup(_toVersion, _versions);
			GUILayout.EndHorizontal();

			GUILayout.Space(5);
			if (GUILayout.Button("Diff"))
				Diff();

			DrawDiff();
		}

		private void DrawDiff()
		{
			if (_diffs == null)
			{
				GUILayout.Label("Press on button \"Diff\"");
				return;
			}

			if (_diffs.All(f => f.DiffType == EDiffType.None))
			{
				GUILayout.Label("No diffs");
				return;
			}

			_scrollPosition = GUILayout.BeginScrollView(_scrollPosition, false, false);
			foreach (var diff in _diffs)
			{
				if (diff.DiffType == EDiffType.None)
					continue;

				DrawDiff(diff);
			}

			GUILayout.EndScrollView();
		}

		private static void DrawDiff(Diff diff)
		{
			var color = GetDiffColor(diff.DiffType);
			GUI.contentColor = color;
			EditorGUILayout.BeginVertical(GUI.skin.box);
			GUILayout.Label(DiffPrefixes[diff.DiffType] + diff.Name);
			if (diff.ChildDiffs.Count > 0)
			{
				GUILayout.BeginHorizontal();
				GUILayout.Space(25);

				GUILayout.BeginVertical();
				foreach (var diffChild in diff.ChildDiffs)
					DrawDiff(diffChild);
				GUILayout.EndVertical();

				GUILayout.Space(5);
				GUILayout.EndHorizontal();
			}

			EditorGUILayout.EndVertical();
		}

		private void Diff()
		{
			var fromVersion = _versions[_fromVersion];
			var toVersion = _versions[_toVersion];
			_diffs = ByteDataDiffService.Diff(fromVersion, toVersion);
		}

		private static Color GetDiffColor(EDiffType diffType)
		{
			Color rgb;
			switch (diffType)
			{
				case EDiffType.None:
					rgb = Color.gray;
					break;
				case EDiffType.Added:
					rgb = Color.green;
					break;
				case EDiffType.Removed:
					rgb = Color.red;
					break;
				case EDiffType.Changed:
					rgb = Color.yellow;
					break;
				case EDiffType.Moved:
					rgb = Color.blue;
					break;
				default:
					throw new ArgumentOutOfRangeException(nameof(diffType), diffType, null);
			}

			// rgb.a = 0.15f;
			return rgb;
		}

		private static Texture2D CreateTexture(int width, int height, Color color)
		{
			var colors = new Color[width * height];
			for (var index = 0; index < colors.Length; ++index)
				colors[index] = color;
			var texture2D = new Texture2D(width, height);
			texture2D.SetPixels(colors);
			texture2D.Apply();
			return texture2D;
		}
	}
}