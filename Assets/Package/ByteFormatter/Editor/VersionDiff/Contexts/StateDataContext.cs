﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Package.ByteFormatter.VersionDiff.Contexts
{
	public class StateDataContext : ADataContext
	{
		public override string Name { get; }
		public override List<ADataContext> ChildContexts { get; }

		public StateDataContext(string name, Type type)
		{
			Name = name;
			ChildContexts = type.GetFields().Select(f => (ADataContext) new StateFieldDataContext(f)).ToList();
		}
	}
}