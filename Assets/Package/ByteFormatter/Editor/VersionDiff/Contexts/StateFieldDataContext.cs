﻿using System.Collections.Generic;
using System.Reflection;
using ByteFormatter.Runtime.MigrateHelper;

namespace Package.ByteFormatter.VersionDiff.Contexts
{
	public class StateFieldDataContext : ADataContext
	{
		public override string Name { get; }
		public override List<ADataContext> ChildContexts { get; }

		public StateFieldDataContext(FieldInfo fieldInfo)
		{
			Name = fieldInfo.Name;
			ChildContexts = new List<ADataContext>
			{
				new FieldTypeDataContext(fieldInfo.FieldType.GetCustomAttribute<ByteDataTypeAccessAttribute>())
			};
		}
	}
}