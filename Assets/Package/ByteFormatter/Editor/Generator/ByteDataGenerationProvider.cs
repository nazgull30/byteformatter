﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using ByteFormatter.Runtime.StateSerialize;
using ByteFormatter.Runtime.Utils;
using Package.ByteFormatter.Generator.MigrateHelper;
using Package.ByteFormatter.Generator.Utils;
using UnityEditor;

namespace Package.ByteFormatter.Generator
{
	public class ByteDataGenerationProvider
	{
		private readonly IByteDataGenerator _generator;

		public ByteDataGenerationProvider(IByteDataGenerator generator)
		{
			_generator = generator;
		}

		public void Generate(DirectoryInfo directoryInfo)
		{
			var byteDataContexts = AssemblyHelper.GetTypes()
				.Where(f => f.HasAttribute<ByteDataContextAttribute>())
				.Select(t => new ByteDataContext(t, t.GetCustomAttribute<ByteDataContextAttribute>()))
				.ToArray();

			GenerateContexts(byteDataContexts);

			if (!directoryInfo.Exists)
				directoryInfo.Create();

			WriteToFile(directoryInfo, byteDataContexts);
			AssetDatabase.Refresh();
		}

		private void GenerateContexts(ByteDataContext[] contexts)
		{
			var failed = new HashSet<string>();
			var generated = new HashSet<string>();
			var sortedContexts = contexts
				.OrderBy(c => c.GetByteConvertableDependencies().Length)
				.ToList();

			var index = 0;
			while (sortedContexts.Count > 0)
			{
				if (failed.Count == sortedContexts.Count)
					break;

				var context = sortedContexts[index];
				var dependencies = context.GetByteConvertableDependencies();
				if (!generated.IsSupersetOf(dependencies))
				{
					failed.Add(context.Type.FullName);
					index++;
					continue;
				}

				GenerateContext(contexts, context);
				sortedContexts.RemoveAt(index);
				generated.Add(context.Type.FullName);
				failed.Clear();
				index = 0;
			}

			foreach (var f in failed)
			{
				UnityEngine.Debug.LogError(
					$"[{nameof(ByteDataGenerationProvider)}] Cannot generate state serialize for type: {f}");
			}
		}

		private void GenerateContext(ByteDataContext[] contexts, ByteDataContext context)
		{
			var dependencies = context.GetByteConvertableDependencies();

			foreach (var dependency in dependencies)
			{
				var dependencyContext = contexts.FirstOrDefault(f => f.Type.FullName == dependency);
				if (dependencyContext == null)
					throw new System.Exception(
						$"[{nameof(MigrateHelperGenerator)}] Cannot find dependency context {dependency}");
				GenerateContext(contexts, dependencyContext);
			}

			var classDeclarationSyntax = _generator.Create(contexts, context);
			context.CompilationUnitSyntax = classDeclarationSyntax;
		}

		private static void WriteToFile(DirectoryInfo directoryInfo, IEnumerable<ByteDataContext> contexts)
		{
			foreach (var context in contexts)
			{
				var path = Path.Combine(directoryInfo.FullName, context.ClassName + ".cs");
				File.WriteAllText(path, context.CompilationUnitSyntax);
			}
		}
	}
}