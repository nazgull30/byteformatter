﻿using System.IO;
using Package.ByteFormatter.ByteValidator;
using UnityEditor;
using UnityEngine;

namespace Package.ByteFormatter.Generator.StateSerialize
{
	public class StateSerializeEditorWindow : EditorWindow
	{
		private const string GENERATION_PATH_KEY = "StateSerialize.Generation.Path";

		private static readonly string DefaultGenerationPath = Path.Combine("Generated", "StateSerialize");

		private string _generationPath;

		[MenuItem("Tools/StateSerialize/Settings")]
		public static void OpenSettingsWindowButton()
		{
			var window =
				GetWindowWithRect<StateSerializeEditorWindow>(new Rect(0, 0, 300, 50), false, "StateSerialize");
			window.Show();
		}

		[MenuItem("Tools/StateSerialize/Generate")]
		public static void GenerateButton() => Generate();

		[MenuItem("Tools/StateSerialize/Validate Serialization")]
		public static void ValidateButton() => ByteConvetableValidator.Validate();

		private void OnEnable()
		{
			_generationPath = GetGenerationPath();
		}

		private void OnGUI()
		{
			EditorGUI.BeginChangeCheck();
			_generationPath = GUILayout.TextField(_generationPath);
			if (EditorGUI.EndChangeCheck())
			{
				SaveGenerationPath();
			}

			if (GUILayout.Button("Generate"))
				Generate();
		}

		private static string GetGenerationPath() => EditorPrefs.GetString(GENERATION_PATH_KEY, DefaultGenerationPath);

		private void SaveGenerationPath()
		{
			EditorPrefs.SetString(GENERATION_PATH_KEY, _generationPath);
		}

		private static void Generate()
		{
			var generationPath = Path.Combine(Application.dataPath, GetGenerationPath());
			var directoryInfo = new DirectoryInfo(generationPath);
			StateSerializeGenerator.Generate(directoryInfo);
		}
	}
}