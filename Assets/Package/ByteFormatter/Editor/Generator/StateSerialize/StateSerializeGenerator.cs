﻿using System.IO;

namespace Package.ByteFormatter.Generator.StateSerialize
{
	public static class StateSerializeGenerator
	{
		public static void Generate(DirectoryInfo directoryInfo)
		{
			var generator = new StateSerializeByteDataGenerator();
			var generationProvider = new ByteDataGenerationProvider(generator);
			generationProvider.Generate(directoryInfo);
		}
	}
}