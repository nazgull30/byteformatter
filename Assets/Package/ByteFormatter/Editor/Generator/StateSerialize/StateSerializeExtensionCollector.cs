﻿using System;
using System.Collections.Generic;
using System.Linq;
using ByteFormatter.Runtime;
using ByteFormatter.Runtime.StateSerialize;
using ByteFormatter.Runtime.Utils;
using Package.ByteFormatter.Generator.Utils;
using UnityEditor;

namespace Package.ByteFormatter.Generator.StateSerialize
{
	[InitializeOnLoad]
	public class StateSerializeExtensionCollector
	{
		private static readonly Dictionary<Type, ExtensionMethod> WriterExtensionMethods;
		private static readonly Dictionary<Type, ExtensionMethod> ReaderExtensionMethods;

		static StateSerializeExtensionCollector()
		{
			var types = AssemblyHelper.GetTypes()
				.Where(t => t.HasAttribute<ByteExtensionAttribute>())
				.ToArray();

			var writerMethods = types.Select(GetWriterMethods)
				.Aggregate((enumerable1, enumerable2) => enumerable1.Concat(enumerable2))
				.ToList();
			WriterExtensionMethods = writerMethods.Distinct().ToDictionary(method => method.SerializedType);
			var writerExtensionMethodsDuplicates = writerMethods.GroupBy(method => method.SerializedType)
				.Where(methods => methods.Count() > 1)
				.ToArray();

			foreach (var duplicate in writerExtensionMethodsDuplicates)
			{
				var type = duplicate.Key;
				var namespaces = duplicate.Select(s => $"{s.Namespace}.{s.ClassName} -> {s.Name}").MergeString();
				UnityEngine.Debug.LogError(
					$"[{nameof(StateSerializeExtensionCollector)}] Duplicate writer extension for type {type.FullName}: \r\n {namespaces}\r\n");
			}

			var readMethods = types.Select(GetReaderMethods)
				.Aggregate((enumerable1, enumerable2) => enumerable1.Concat(enumerable2))
				.ToList();
			ReaderExtensionMethods = readMethods.Distinct().ToDictionary(method => method.SerializedType);
			var readerExtensionMethodsDuplicates = readMethods.GroupBy(method => method.SerializedType)
				.Where(methods => methods.Count() > 1)
				.ToArray();

			foreach (var duplicate in readerExtensionMethodsDuplicates)
			{
				var type = duplicate.Key;
				var namespaces = duplicate.Select(s => $"{s.Namespace}.{s.ClassName} -> {s.Name}").MergeString();
				UnityEngine.Debug.LogError(
					$"[{nameof(StateSerializeExtensionCollector)}] Duplicate read extension for type {type.FullName}: \r\n {namespaces}\r\n");
			}
		}

		private static IEnumerable<ExtensionMethod> GetWriterMethods(Type type)
		{
			var methods1 = type.GetMethods()
				.Where(f => f.GetParameters().Length == 1)
				.Where(m => m.Name.StartsWith("Write", StringComparison.Ordinal) && m.ReturnType == typeof(void))
				.Select(s => new ExtensionMethod(type.Namespace, type.Name, s.Name,
					s.GetParameters()[0].ParameterType));

			var methods2 = type.GetMethods()
				.Where(f => f.GetParameters().Length == 2)
				.Where(m => m.Name.StartsWith("Write", StringComparison.Ordinal)
				            && m.ReturnType == typeof(void)
				            && m.GetParameters()[0].ParameterType == typeof(ByteWriter)
				)
				.Select(s => new ExtensionMethod(type.Namespace, type.Name, s.Name,
					s.GetParameters()[1].ParameterType));

			return methods1.Concat(methods2);
		}

		public static ExtensionMethod GetWriterMethodForType(Type type)
			=> WriterExtensionMethods.TryGetValue(type, out var method) ? method : null;

		public static ExtensionMethod GetReaderMethodForType(Type type)
			=> ReaderExtensionMethods.TryGetValue(type, out var method) ? method : null;

		private static IEnumerable<ExtensionMethod> GetReaderMethods(Type type)
			=> type.GetMethods()
				.Where(m => m.Name.StartsWith("Read", StringComparison.Ordinal) &&
				            (m.GetParameters().Length == 0
				             || m.IsStatic && m.GetParameters()[0].ParameterType == typeof(ByteReader)))
				.Select(s => new ExtensionMethod(type.Namespace, type.Name, s.Name, s.ReturnType));

		public class ExtensionMethod
		{
			public readonly string Namespace;
			public readonly string ClassName;
			public readonly string Name;
			public readonly Type SerializedType;

			public ExtensionMethod(string ns, string className, string name, Type serializedType)
			{
				Namespace = ns;
				ClassName = className;
				Name = name;
				SerializedType = serializedType;
			}

			protected bool Equals(ExtensionMethod other) => SerializedType == other.SerializedType;

			public override bool Equals(object obj)
			{
				if (ReferenceEquals(null, obj)) return false;
				if (ReferenceEquals(this, obj)) return true;
				if (obj.GetType() != this.GetType()) return false;
				return Equals((ExtensionMethod) obj);
			}

			public override int GetHashCode() => (SerializedType != null ? SerializedType.GetHashCode() : 0);
		}
	}
}