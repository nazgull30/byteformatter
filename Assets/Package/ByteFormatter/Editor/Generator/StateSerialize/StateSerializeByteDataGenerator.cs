﻿using System;
using System.Collections;
using System.Linq;
using System.Reflection;
using ByteFormatter.Runtime;
using ByteFormatter.Runtime.StateSerialize;
using ByteFormatter.Runtime.Utils;
using Package.ByteFormatter.Generator.MigrateHelper;

namespace Package.ByteFormatter.Generator.StateSerialize
{
	public class StateSerializeByteDataGenerator : IByteDataGenerator
	{
		public string Create(ByteDataContext[] contexts, ByteDataContext context)
		{
			context.ClassName = context.Type.Name;
			var writeMethods = GetWriteMethods(context);
			var readMethods = GetReadMethods(context);

			var usingDirectives = context.GetNamespaces()
				.Union(new[]
				{
					typeof(IByteConvertable).Namespace,
					typeof(ByteReader).Namespace,
					typeof(ByteWriter).Namespace,
				})
				.Concat(writeMethods.Select(s => s.Namespace))
				.Concat(readMethods.Select(s => s.Namespace))
				.Distinct()
				.Where(s => !string.IsNullOrEmpty(s))
				.Select(StateSerializeGeneratorTemplates.Using)
				.MergeString();

			var writeBlock = writeMethods
				.Select(GetMethodStatement)
				.MergeString();

			var readBlock = readMethods
				.Select(s => StateSerializeGeneratorTemplates.ReadMethod(s.ArgumentName, s.MethodName))
				.MergeString();

			return StateSerializeGeneratorTemplates.Class(
				usingDirectives,
				context.Type.Namespace,
				context.ClassName,
				writeBlock,
				readBlock
			);
		}

		private static string GetMethodStatement(GeneratedMethod method)
			=> method.IsByteConvertable
				? StateSerializeGeneratorTemplates.WriteByteConvertableMethod(method.MethodName, method.ArgumentName)
				: StateSerializeGeneratorTemplates.WriteMethod(method.MethodName, method.ArgumentName);

		private static GeneratedMethod[] GetWriteMethods(ByteDataContext context)
			=> context.GetProperties()
				.Where(p => p.HasAttribute<ByteDataIndexAttribute>())
				.OrderBy(p => p.GetCustomAttribute<ByteDataIndexAttribute>().Index)
				.Select(GetWriteMethod)
				.ToArray();

		private static GeneratedMethod GetWriteMethod(PropertyInfo propertyInfo)
		{
			var generatedMethod = new GeneratedMethod {ArgumentName = propertyInfo.Name};
			var propertyType = propertyInfo.PropertyType;
			if (IsIByteConvertableList(propertyType) || IsIByteConvertable(propertyType))
			{
				generatedMethod.MethodName = "Write";
				generatedMethod.IsByteConvertable = true;
				return generatedMethod;
			}

			var extensionMethod = StateSerializeExtensionCollector.GetWriterMethodForType(propertyType);
			if (extensionMethod == null)
				throw new Exception(
					$"[{nameof(StateSerializeByteDataGenerator)}] No write method for type {propertyType}");
			generatedMethod.Namespace = extensionMethod.Namespace;
			generatedMethod.MethodName = extensionMethod.Name;
			return generatedMethod;
		}

		private static GeneratedMethod[] GetReadMethods(ByteDataContext context)
			=> context.GetProperties()
				.Where(p => p.HasAttribute<ByteDataIndexAttribute>())
				.OrderBy(p => p.GetCustomAttribute<ByteDataIndexAttribute>().Index)
				.Select(GetReadMethod)
				.ToArray();

		private static GeneratedMethod GetReadMethod(PropertyInfo propertyInfo)
		{
			var generatedMethod = new GeneratedMethod {ArgumentName = propertyInfo.Name};
			var propertyType = propertyInfo.PropertyType;

			if (IsIByteConvertableList(propertyType))
			{
				var genericArgument = propertyType.GetGenericArguments()[0];
				generatedMethod.MethodName = $"ReadList<{genericArgument.Name}>";
				return generatedMethod;
			}

			if (IsIByteConvertable(propertyType))
			{
				generatedMethod.MethodName = $"Read<{propertyType.Name}>";
				return generatedMethod;
			}

			var extensionMethod = StateSerializeExtensionCollector.GetReaderMethodForType(propertyType);
			if (extensionMethod == null)
				throw new Exception(
					$"[{nameof(StateSerializeByteDataGenerator)}] No read method for type {propertyType}");
			generatedMethod.Namespace = extensionMethod.Namespace;
			generatedMethod.MethodName = extensionMethod.Name;
			return generatedMethod;
		}

		private static bool IsIByteConvertable(Type type)
			=> type.HasAttribute<ByteDataContextAttribute>();

		private static bool IsIByteConvertableList(Type type)
			=> type.ImplementsInterface<IList>()
			   && type.GetGenericArguments().Length == 1
			   && IsIByteConvertable(type.GetGenericArguments()[0]);

		private class GeneratedMethod
		{
			public string Namespace;
			public string MethodName;
			public string ArgumentName;
			public bool IsByteConvertable;
		}
	}
}