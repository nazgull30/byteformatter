﻿namespace Package.ByteFormatter.ByteValidator.Impls.Providers
{
	[ByteValidatorDataProvider]
	public sealed class BoolDataProvider : ADataProvider<bool>
	{
		protected override bool[] DataArray { get; } = {true};
		protected override int[] DaraSizeArray { get; } = {1};
	}
}