﻿using UnityEngine;

namespace Package.ByteFormatter.ByteValidator.Impls.Providers
{
	[ByteValidatorDataProvider]
	public sealed class QuaternionDataProvider : ADataProvider<Quaternion>
	{
		protected override Quaternion[] DataArray { get; } = {Quaternion.identity};
		protected override int[] DaraSizeArray { get; } = {16};
	}
}