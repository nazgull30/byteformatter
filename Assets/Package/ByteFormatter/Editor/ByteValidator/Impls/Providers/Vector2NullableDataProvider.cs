﻿using UnityEngine;

namespace Package.ByteFormatter.ByteValidator.Impls.Providers
{
	[ByteValidatorDataProvider]
	public sealed class Vector2NullableDataProvider : ADataProvider<Vector2?>
	{
		protected override Vector2?[] DataArray { get; } = {null, Vector2.one};
		protected override int[] DaraSizeArray { get; } = {1, 9};
	}
}