namespace Package.ByteFormatter.ByteValidator.Impls.Providers
{
	[ByteValidatorDataProvider]
	public sealed class UIntNullableDataProvider : ADataProvider<uint?>
	{
		protected override uint?[] DataArray { get; } = { null, uint.MaxValue };
		protected override int[] DaraSizeArray { get; } = { 1, 5 };
	}
}