﻿namespace Package.ByteFormatter.ByteValidator.Impls.Providers
{
	[ByteValidatorDataProvider]
	public sealed class FloatNullableDataProvider : ADataProvider<float?>
	{
		protected override float?[] DataArray { get; } = {null, float.MaxValue};
		protected override int[] DaraSizeArray { get; } = {1, 5};
	}
}