﻿namespace Package.ByteFormatter.ByteValidator.Impls.Providers
{
	[ByteValidatorDataProvider]
	public sealed class IntNullableDataProvider : ADataProvider<int?>
	{
		protected override int?[] DataArray { get; } = {null, int.MaxValue};
		protected override int[] DaraSizeArray { get; } = {1, 5};
	}
}