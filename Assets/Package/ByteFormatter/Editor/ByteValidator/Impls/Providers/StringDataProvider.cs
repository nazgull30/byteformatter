﻿using System.Text;

namespace Package.ByteFormatter.ByteValidator.Impls.Providers
{
	[ByteValidatorDataProvider]
	public sealed class StringDataProvider : ADataProvider<string>
	{
		protected override string[] DataArray { get; } = {"THIS IS SPARTA!"};

		protected override int[] DaraSizeArray => new[]
		{
			(4 + Encoding.UTF8.GetBytes("THIS IS SPARTA!").Length)
		};
	}
}