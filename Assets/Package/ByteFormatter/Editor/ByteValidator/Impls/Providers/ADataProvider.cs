﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace Package.ByteFormatter.ByteValidator.Impls.Providers
{
	public abstract class ADataProvider<T> : IDataProvider
	{
		protected abstract T[] DataArray { get; }

		protected abstract int[] DaraSizeArray { get; }

		public int GetDataVariants(Type type) => DataArray.Length;

		public virtual bool CanProvide(Type type) => type == typeof(T);

		public int GetDataSize(int variantNumber, Type type) => DaraSizeArray[variantNumber];

		public object GetData(int variantNumber, Type type) => DataArray[variantNumber];

		public void SetData(int variantNumber, object obj, PropertyInfo propertyInfo)
			=> propertyInfo.SetValue(obj, DataArray[variantNumber]);

		public virtual List<string> AssertDataEqual(int variantNumber, object obj, string name)
		{
			if (DataArray[variantNumber].Equals(obj))
				return new List<string>();
			return new List<string>
			{
				$"Data in object {obj.GetType().Name} in property {name} not valid"
			};
		}
	}
}