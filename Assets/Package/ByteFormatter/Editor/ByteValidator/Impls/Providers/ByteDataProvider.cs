﻿namespace Package.ByteFormatter.ByteValidator.Impls.Providers
{
	[ByteValidatorDataProvider]
	public sealed class ByteDataProvider : ADataProvider<byte>
	{
		protected override byte[] DataArray { get; } = { 255 };
		protected override int[] DaraSizeArray { get; } = { 1 };
	}
}