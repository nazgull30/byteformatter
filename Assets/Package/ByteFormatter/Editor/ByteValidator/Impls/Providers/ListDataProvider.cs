﻿using System;
using System.Collections;
using System.Collections.Generic;
using ByteFormatter.Runtime.Utils;

namespace Package.ByteFormatter.ByteValidator.Impls.Providers
{
	[ByteValidatorDataProvider]
	public sealed class ListDataProvider : AIListDataProvider
	{
		public override bool CanProvide(Type type)
			=> !type.IsArray && type.ImplementsInterface<IList>();

		protected override Type GetDataType(Type type) => type.GetGenericArguments()[0];

		protected override IList CreateInstance(Type type, int lenght)
		{
			var listType = typeof(List<>);
			var genericType = listType.MakeGenericType(type);
			var instance = (IList) Activator.CreateInstance(genericType);
			for (var i = 0; i < lenght; i++)
			{
				instance.Add(null);
			}

			return instance;
		}
	}
}