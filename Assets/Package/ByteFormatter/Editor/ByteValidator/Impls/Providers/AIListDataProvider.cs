﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;

namespace Package.ByteFormatter.ByteValidator.Impls.Providers
{
	public abstract class AIListDataProvider : IDataProvider
	{
		public int GetDataVariants(Type type) => 1;

		public abstract bool CanProvide(Type type);

		public int GetDataSize(int variantNumber, Type type)
		{
			var elementType = GetDataType(type);
			var dataVariants = DataProviderService.GetDataVariants(elementType);
			var dataSize = 4;
			for (var i = 0; i < dataVariants; i++)
			{
				dataSize += DataProviderService.GetDataSize(i, elementType);
			}

			return dataSize;
		}

		public object GetData(int variantNumber, Type type)
		{
			var elementType = GetDataType(type);
			var dataVariants = DataProviderService.GetDataVariants(elementType);
			var instance = CreateInstance(elementType, dataVariants);
			for (var i = 0; i < dataVariants; i++)
			{
				var item = DataProviderService.GetData(i, elementType);
				instance[i] = item;
			}

			return instance;
		}

		protected abstract Type GetDataType(Type type);

		protected abstract IList CreateInstance(Type type, int lenght);

		public void SetData(int variantNumber, object obj, PropertyInfo propertyInfo)
		{
			var instance = GetData(variantNumber, propertyInfo.PropertyType);
			propertyInfo.SetValue(obj, instance);
		}

		public List<string> AssertDataEqual(int variantNumber, object obj, string name)
		{
			if (!(obj is IList list))
				throw new System.Exception(
					$"[{nameof(Vector3DataProvider)}] Data do not provide List");

			var errors = new List<string>();
			for (var i = 0; i < list.Count; i++)
			{
				var item = list[i];
				errors.AddRange(DataProviderService.AssertDataEqual(i, item));
			}

			return errors;
		}
	}
}