﻿namespace Package.ByteFormatter.ByteValidator.Impls.Providers
{
	[ByteValidatorDataProvider]
	public sealed class IntDataProvider : ADataProvider<int>
	{
		protected override int[] DataArray { get; } = {int.MaxValue};
		protected override int[] DaraSizeArray { get; } = {4};
	}
}