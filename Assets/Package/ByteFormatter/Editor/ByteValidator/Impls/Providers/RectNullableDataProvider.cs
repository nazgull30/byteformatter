﻿using UnityEngine;

namespace Package.ByteFormatter.ByteValidator.Impls.Providers
{
	[ByteValidatorDataProvider]
	public sealed class RectNullableDataProvider : ADataProvider<Rect?>
	{
		protected override Rect?[] DataArray { get; } = {null, new Rect(1, 1, 1, 1)};
		protected override int[] DaraSizeArray { get; } = {1, 17};
	}
}