﻿using System;
using System.Collections.Generic;
using System.Reflection;
using ByteFormatter.Runtime.StateSerialize;

namespace Package.ByteFormatter.ByteValidator.Impls.Providers
{
	[ByteValidatorDataProvider]
	public sealed class IByteConvertableDataProvider : IDataProvider
	{
		public int GetDataVariants(Type type)
			=> DataProviderService.GetDataVariants(type);

		public bool CanProvide(Type type)
			=> type.GetInterface(nameof(IByteConvertable)) != null;

		public int GetDataSize(int variantNumber, Type type)
			=> DataProviderService.GetDataSize(variantNumber, type);

		public object GetData(int variantNumber, Type type)
		{
			var value = Activator.CreateInstance(type);
			DataProviderService.SetData(variantNumber, value);
			return value;
		}

		public void SetData(int variantNumber, object obj, PropertyInfo propertyInfo)
		{
			var value = GetData(variantNumber, propertyInfo.PropertyType);
			propertyInfo.SetValue(obj, value);
		}

		public List<string> AssertDataEqual(int variantNumber, object obj, string name)
			=> DataProviderService.AssertDataEqual(variantNumber, obj);
	}
}