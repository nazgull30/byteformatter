namespace Package.ByteFormatter.ByteValidator.Impls.Providers
{
	[ByteValidatorDataProvider]
	public sealed class ULongNullableDataProvider : ADataProvider<ulong?>
	{
		protected override ulong?[] DataArray { get; } = { null, ulong.MaxValue };
		protected override int[] DaraSizeArray { get; } = { 1, 9 };
	}
}