namespace Package.ByteFormatter.ByteValidator.Impls.Providers
{
	[ByteValidatorDataProvider]
	public sealed class ShortNullableDataProvider : ADataProvider<short?>
	{
		protected override short?[] DataArray { get; } = { null, short.MaxValue };
		protected override int[] DaraSizeArray { get; } = { 1, 3 };
	}
}