﻿namespace Package.ByteFormatter.ByteValidator.Impls.Providers
{
	[ByteValidatorDataProvider]
	public sealed class LongNullableDataProvider : ADataProvider<long?>
	{
		protected override long?[] DataArray { get; } = {null, long.MaxValue};
		protected override int[] DaraSizeArray { get; } = {1, 9};
	}
}