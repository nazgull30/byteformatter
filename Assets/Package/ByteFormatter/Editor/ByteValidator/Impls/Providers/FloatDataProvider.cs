﻿namespace Package.ByteFormatter.ByteValidator.Impls.Providers
{
	[ByteValidatorDataProvider]
	public sealed class FloatDataProvider : ADataProvider<float>
	{
		protected override float[] DataArray { get; } = {float.MaxValue};
		protected override int[] DaraSizeArray { get; } = {4};
	}
}