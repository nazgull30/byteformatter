﻿using System;

namespace Package.ByteFormatter.ByteValidator
{
	[AttributeUsage(AttributeTargets.Class)]
	public class ByteValidatorDataProviderAttribute : Attribute
	{
	}
}